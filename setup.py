from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("wgf3.py")
)

# This is cython compiller command
# for run type: python setup.py build_ext --inplace