#!/usr/local/bin/python2.7

import sys
import os
import numpy as np
from array import array
import struct


class WGF3:

    def __init__(self):
        self.__wgf3 = False

        self.__pLatMin = False
        self.__pLatMax = False
        self.__pLonMin = False
        self.__pLonMax = False
        self.__pDLat = False
        self.__pDLon = False
        self.__pEmptyValue = False

    def open(self, filename):
        try:
            self.__filename = filename
            self.__wgf3 = open(filename, "rb+")
            self.read_header()
            self.read_body()
            self.__wgf_already_exists = True
            #print(self.__data)
        except IOError:
            raise NameError('Error: File does not exist.')
        return True

    def create(self, filename, pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue):
        self.__filename = filename
        self.__wgf3 = open(filename, "wb")
        header = array('i', [pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon])
        self.__wgf3.write(header)
        emptyValue = array('f', [pEmptyValue])
        self.__wgf3.write(emptyValue)
        self.set_header(pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue)

        self.__data = self.get_empty_wgf()
        self.__wgf_already_exists = False
        return True

    def write(self, lat, lon, value):
        idx = self.get_latlon_index(lat, lon)
        #print("lat:" + str(lat) + " lon:" + str(lon) + " idx:" + str(idx) + " val:" + str(value))
        if(idx == -1):
            return False
        self.__data[idx] = value
        return True

    def read(self, lat, lon):
        idx = self.get_latlon_index(lat, lon)
        if(idx == -1):
            return self.__pEmptyValue
        return self.__data[idx]

    def save(self):
        wgf_list = self.__data
        if(self.__wgf_already_exists == True):
            self.create(self.__filename,
                        self.__pLatMin,
                        self.__pLatMax,
                        self.__pLonMin,
                        self.__pLonMax,
                        self.__pDLat,
                        self.__pDLon,
                        self.__pEmptyValue)

        ar = array('f', wgf_list)
        self.__wgf3.write(ar)
        self.__wgf3.close()
        self.flush()
        return True

    def flush(self):
        self.__wgf3.close()

        self.__pLatMin = False
        self.__pLatMax = False
        self.__pLonMin = False
        self.__pLonMax = False
        self.__pDLat = False
        self.__pDLon = False
        self.__pEmptyValue = False
        self.__data = False
        return True

    def __del__(self):
        self.__wgf3.close()

    def get_empty_wgf(self):
        return np.full(self.width() * self.height(), self.__pEmptyValue, np.float).tolist()

    def get_empty(self):
        return self.__pEmptyValue

    def get_header(self):
        return {'pLatMin': self.__pLatMin,
                'pLatMax': self.__pLatMax,
                'pLonMin': self.__pLonMin,
                'pLonMax': self.__pLonMax,
                'pDLat': self.__pDLat,
                'pDLon': self.__pDLon,
                'pEmptyValue': self.__pEmptyValue}

    def set_header(self, pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue):
        self.__pLatMin = pLatMin
        self.__pLatMax = pLatMax
        self.__pLonMin = pLonMin
        self.__pLonMax = pLonMax
        self.__pDLat = pDLat
        self.__pDLon = pDLon
        self.__pEmptyValue = pEmptyValue

    def read_header(self):
        struct_fmt = '=iiiiiif'
        struct_len = struct.calcsize(struct_fmt)
        struct_unpack = struct.Struct(struct_fmt).unpack_from

        self.__wgf3.seek(0)
        data = self.__wgf3.read(struct_len)
        s = struct_unpack(data)
        self.set_header(s[0], s[1], s[2], s[3], s[4], s[5], s[6])
        return self.get_header()

    def read_body(self):
        struct_fmt = '=iiiiiif'
        struct_len = struct.calcsize(struct_fmt)

        self.__wgf3.seek(struct_len)
        self.__data = np.fromstring(self.__wgf3.read(), dtype='<f4').tolist()

    def width(self):
        return (self.__pLonMax - self.__pLonMin) / self.__pDLon + 1

    def height(self):
        return (self.__pLatMax - self.__pLatMin) / self.__pDLat + 1

    def get_latlon_index(self, lat, lon):
        if(lon > 180000):
            lon = (lon + 180000) % 360000 - 180000
        if(lon < -180000):
            lon = (lon + 180000) % 360000 + 180000

        if(lon < self.__pLonMin):
            return -1
        if(lon > self.__pLonMax):
            return -1
        if(lat < self.__pLatMin):
            return -1
        if(lat > self.__pLatMax):
            return -1

        col = round((lon - self.__pLonMin) / float(self.__pDLon))
        row = round((lat - self.__pLatMin) / float(self.__pDLat))
        idx = (self.width() * row) + col
        #print("lat:" + str(lat) + " lon:" + str(lon) + " x:" + str(x) + " col:" + str(col) + " row:" + str(row) + " r:" + str(r) + " idx:" + str(idx))
        return int(idx)

class Wgf3Test:
    def test_read_write(self):
        wgf3 = WGF3()
        filename = '/tmp/wgftest.wgf3'

        latMin = -179500
        latMax = 180000
        lonMin = -90000
        lonMax = 90000
        dLat = 100
        dLon = 100
        emptyValue = -100500.0

        wgf3.create(filename, latMin, latMax, lonMin, lonMax, dLat, dLon, emptyValue)
        for lat in range(latMin, latMax, dLat):
            for lon in range(lonMin, lonMax, dLon):
                orig_val = (lat * 1000.0 + lon) / 1000.0
                wgf3.write(lat, lon, orig_val)

        wgf3.save()
        print("File saved")
        wgf3.open(filename)
        print("Open file " + str(filename))
        for lat in range(latMin, latMax, dLat):
            for lon in range(lonMin, lonMax, dLon):
                orig_val = (lat * 1000.0 + lon) / 1000.0
                readed_val = wgf3.read(lat, lon)
                if( orig_val != readed_val):
                    print("Error for lat:" + str(lat) + " lon:" + str(lon) + " Expected value:" + str(orig_val) + " real value:" + str(readed_val))
        print("File checked")

        wgf3.flush()
        return True
'''
test = Wgf3Test()
test.test_read_write()

w = WGF3()
f = '/windy_datasource/data/gwes/06.04.2018_00:00_1522972800/HTSGW.wgf3'
w.open(f)
print(w.get_header())
w.flush()
f = '/windy_datasource/data/gwes/HTSGW15.wgf3'
w.create(f, -179500, 180000, -80000, 80000, 500, 500, -100500.0)
w.write(110300, 10100, 2.56)
w.save()

w.open(f)
print(w.read(110300, 10100))
print(w.read(110400, 10100))
print(w.read(110600, 10100))
'''