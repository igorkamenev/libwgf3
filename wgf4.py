#!/usr/local/bin/python2.7

import sys
import os
import numpy as np
from array import array
import struct


class WGF4:

    def __init__(self):
        self.__wgf4 = False

        self.__pLatMin = False
        self.__pLatMax = False
        self.__pLonMin = False
        self.__pLonMax = False
        self.__pDLat = False
        self.__pDLon = False
        self.__pEmptyValue = False
        self.__pStep = False
        self.__width = -1
        self.__height = -1

    def open(self, filename):
        try:
            self.__filename = filename
            self.__wgf4 = open(filename, "rb+")
            self.read_header()
            self.read_body()
            self.__wgf_already_exists = True
        except IOError:
            raise NameError('Error: File does not exist.')
        return True

    def create(self, filename, pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue, pStep):
        self.__filename = filename
        self.__wgf4 = open(filename, "wb")
        step = array('i', [pStep])
        self.__wgf4.write(step)
        header = array('f', [pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue])
        self.__wgf4.write(header)
        self.set_header(pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue, pStep)
        self.__data = self.get_empty_wgf()
        self.__wgf_already_exists = False
        return True

    def write(self, lat, lon, value):
        idx = self.get_latlon_index(lat, lon)
        if(idx == -1):
            return False
        self.__data[idx] = value
        return True

    def read(self, lat, lon):
        idx = self.get_latlon_index(lat, lon)
        if(idx == -1):
            return self.__pEmptyValue
        return round(self.__data[idx], 3)

    def save(self):
        wgf_list = self.__data
        if(self.__wgf_already_exists == True):
            self.create(self.__filename,
                        self.__pLatMin,
                        self.__pLatMax,
                        self.__pLonMin,
                        self.__pLonMax,
                        self.__pDLat,
                        self.__pDLon,
                        self.__pEmptyValue,
                        self.__pStep)

        ar = array('f', wgf_list)
        self.__wgf4.write(ar)
        self.__wgf4.close()
        self.flush()
        return True

    def flush(self):
        self.__wgf4.close()

        del self.__pLatMin
        del self.__pLatMax
        del self.__pLonMin
        del self.__pLonMax
        del self.__pDLat
        del self.__pDLon
        del self.__pEmptyValue
        del self.__pStep
        del self.__data
        return True

    def __del__(self):
        self.__wgf4.close()

    def get_empty_wgf(self):
        return np.full(self.width() * self.height(), self.__pEmptyValue, np.float).tolist()

    def get_empty(self):
        return self.__pEmptyValue

    def get_header(self):
        return {'pLatMin': self.__pLatMin,
                'pLatMax': self.__pLatMax,
                'pLonMin': self.__pLonMin,
                'pLonMax': self.__pLonMax,
                'pDLat': self.__pDLat,
                'pDLon': self.__pDLon,
                'pEmptyValue': self.__pEmptyValue,
                'pStep': self.__pStep}

    def set_header(self, pLatMin, pLatMax, pLonMin, pLonMax, pDLat, pDLon, pEmptyValue, pStep):
        self.__pLatMin = pLatMin
        self.__pLatMax = pLatMax
        self.__pLonMin = pLonMin
        self.__pLonMax = pLonMax
        self.__pDLat = pDLat
        self.__pDLon = pDLon
        self.__pEmptyValue = pEmptyValue
        self.__pStep = pStep

    def read_header(self):
        struct_fmt = '=ifffffff'
        struct_len = struct.calcsize(struct_fmt)
        struct_unpack = struct.Struct(struct_fmt).unpack_from

        self.__wgf4.seek(0)
        data = self.__wgf4.read(struct_len)
        s = struct_unpack(data)
        self.set_header(s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[0])
        return self.get_header()

    def read_body(self):
        struct_fmt = '=ifffffff'
        struct_len = struct.calcsize(struct_fmt)

        self.__wgf4.seek(struct_len)
        self.__data = np.fromstring(self.__wgf4.read(), dtype='<f4').tolist()

    def width(self):
        if( self.__width > -1 ):
            return self.__width
        self.__width = int(((self.__pLonMax - self.__pLonMin) * self.__pStep) / int(self.__pDLon * self.__pStep) + 1)
        return self.__width

    def height(self):
        if( self.__height > -1 ):
            return self.__height
        self.__height = int(((self.__pLatMax - self.__pLatMin) * self.__pStep) / int(self.__pDLat * self.__pStep) + 1)
        return self.__height

    def get_latlon_index(self, lat, lon):
        if(lon > 180):
            lon = (lon + 180) % 360 - 180
        if(lon < -180):
            lon = (lon + 180) % 360 + 180

        if(lon < self.__pLonMin):
            return -1
        if(lon > self.__pLonMax):
            return -1
        if(lat < self.__pLatMin):
            return -1
        if(lat > self.__pLatMax):
            return -1

        col = ((lon - self.__pLonMin) * self.__pStep) / int(self.__pDLon * self.__pStep)
        row = ((lat - self.__pLatMin) * self.__pStep) / int(self.__pDLat * self.__pStep)
        idx = (self.width() * row) + col
        #print("width:" + str(self.width()) + " lat:" + str(lat) + " lon:" + str(lon) + " col:" + str(col) + " row:" + str(row) + " idx:" + str(idx) + " int(idx):" + str(int(round(idx))))
        return int(round(idx))

class Wgf4Test:
    def test_read_write(self):
        wgf4 = WGF4()
        filename = '/tmp/wgftest.wgf4'

        latMin = -180.5
        latMax = 180.0
        lonMin = -90.0
        lonMax = 90.0
        dLat = 0.1
        dLon = 0.1
        emptyValue = -100500.0
        step = 1000

        wgf4.create(filename, latMin, latMax, lonMin, lonMax, dLat, dLon, emptyValue, step)
        for lat in range(int(latMin * step), int(latMax * step) +1, int(dLat * step)):
            for lon in range(int(lonMin * step), int(lonMax * step) + 1, int(dLon * step)):
                orig_val = (lat + lon)/float(step)
                wgf4.write(lat/float(step), lon/float(step), orig_val)

        wgf4.save()
        print("File saved")
        wgf4.open(filename)
        print("Open file " + str(filename))
        for lat in range(int(latMin * step), int(latMax * step) + 1, int(dLat * step)):
            for lon in range(int(lonMin * step), int(lonMax * step) + 1, int(dLon * step)):
                orig_val = (lat + lon)/float(step)
                readed_val = wgf4.read(lat/float(step), lon/float(step))
                if( orig_val != readed_val):
                    print("Error for lat:" + str(lat/float(step)) + " lon:" + str(lon/float(step)) + " Expected value:" + str(orig_val) + " real value:" + str(readed_val))
        print("File checked")

        wgf4.flush()
        return True

'''
test = Wgf4Test()
test.test_read_write()

w = WGF4()
f = '/windy_datasource/data/gwes/06.04.2018_00:00_1522972800/HTSGW.wgf3'
w.open(f)
print(w.get_header())
w.flush()
f = '/windy_datasource/data/gwes/HTSGW15.wgf3'
w.create(f, -179500, 180000, -80000, 80000, 500, 500, -100500.0)
w.write(110300, 10100, 2.56)
w.save()

w.open(f)
print(w.read(110300, 10100))
print(w.read(110400, 10100))
print(w.read(110600, 10100))
'''